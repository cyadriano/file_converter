<?php 
namespace Console;

use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Command\Command as SymfonyCommand;

use Symfony\Component\Yaml\Yaml;

/**
 * Author: Chidume Nnamdi <kurtwanger40@gmail.com>
 */
class Command extends SymfonyCommand
{
    public $isHttp = 0;   
    public $newLoc = 0;
    public $file;
    public function __construct()
    {
        parent::__construct();
    }
    public function setHttp($isHttp)
    {
        $this->isHttp = $isHttp;
    }
    public function setNewFileLoc($newLoc)
    {
        $this->newLoc = $newLoc;
    }
    public function setFile($file)
    {
        $this->file = $file;
        
    }
    protected function convertFile(InputInterface $input, OutputInterface $output, $format, $ext)
    {
        $file = $this->file;
        $out  = $this->convert($file, $format, $ext);

        return $out;
    }
    private function convert($file, $format, $ext) 
    {
        switch ($format) {
            
            case 'csv':
                
                // FILEPATH
                $filepath = pathinfo($file);
                if ($this->isHttp) {
                    $newFileLoc = $this->newLoc;
                    $newFileLoc = $newFileLoc.$filepath['filename'].'.'.$format;
                } else {
                    $newFileLoc = $filepath['dirname'].'/'.$filepath['filename'].'.'.$format;
                }

                // CHECK LOCATION EXISTS
                if(!file_exists(dirname($newFileLoc))) {
                    mkdir(dirname($newFileLoc), 0777, true);
                }

                // GET THE ARRAY 
                if ($ext == 'json') {
                    $getFile = file_get_contents($file);
                    $array = json_decode($getFile);
                } else {
                    $array = Yaml::parse(file_get_contents($file));
                }

                // INSERT EACH ROW TO CSV FILE

                $fp = fopen($newFileLoc, 'w'); 
                
                // Loop through file pointer and a line 
                foreach ($array as $fields) { 
                    fputcsv($fp, $fields); 
                } 
                
                fclose($fp); 

                $data = 'csv';
            break;
            case 'json':
                $array = $this->getArray($file);

                if ($ext == 'csv') {
                    $data = json_encode($array);
                } else {
                    $data = Yaml::parse(file_get_contents($file));
                    $data = json_encode($data);
                }
                break;
            case 'yaml':
                $data = '';
                if ($ext == 'csv') {
                    $array = $this->getArray($file);
                } else {
                    $getFile = file_get_contents($file);
                    $array = json_decode($getFile);
                }
                
                $data = Yaml::dump($array);
                break;
            
            default:
                $data = '';
                break;
        }

        return $data;
    }
    public function getArray($file)
    {
        $getFile = file_get_contents($file);
        $exploded = explode("\n", $getFile);
        return array_map("str_getcsv", $exploded);
    }
}