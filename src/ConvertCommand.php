<?php 
namespace Console;

use Console\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ChoiceQuestion;
use Symfony\Component\Console\Question\Question;

class ConvertCommand extends Command
{
    
    public function configure()
    {
        $this->setName('app:conversion')
            ->addArgument(
                'filename', 
                InputArgument::REQUIRED, 
                'The file you want to read.'
            )
            ->addOption(
                'option',
                null, 
                InputOption::VALUE_REQUIRED, 
                'Options could be read or convert.',
            )
        ;
        
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $formatOpt = [
            'csv' => 'CSV',
            'json' => 'JSON',
            'yaml' => 'YAML',
            'exit' => 'EXIT'
        ];

        $option = $input->getOption('option');
        // CHECK OPTION
        switch ($option) {
            case 'convert':
                $is_http = 1;
                // CHECK FILE FORMAT
                $filename = $input->getArgument('filename');

                $file_headers = @get_headers($filename);
                if(!$file_headers || $file_headers[0] == 'HTTP/1.1 404 Not Found') {
                    $is_http = 0;
                }
                
                // // CHECK FILE IF OVER HTTP OR LOCALPATH
                // if($is_http) {
                //     $client = new \GuzzleHttp\Client();
                //     $filename = $client->request('GET', $filename);
                // } 

                // GET EXTENSION
                $ext = pathinfo($input->getArgument('filename'), PATHINFO_EXTENSION);

                // REMOVE THE FILE FORMAT IN THE OPTION
                unset($formatOpt[$ext]);

                $helper = $this->getHelper('question');
                $question = new ChoiceQuestion(
                    'Please select the new format of the file below:',
                    $formatOpt
                );
                
                // QUESTION FOR NEW FORMAT
                $question->setErrorMessage('"%s" is invalid option. Try the options above.');
                $format = $helper->ask($input, $output, $question);

                // NEW FILENAME
                $filepath = pathinfo($filename);
                if ($is_http) {
                    $helperLoc = $this->getHelper('question');
                    $questionLoc = new Question(
                        'Please enter the file path to store the converted file (<info> ex. /home/user/Desktop/ </info>) = '
                    );
                    
                    $fileLoc = $helperLoc->ask($input, $output, $questionLoc);
                    $this->setHttp($is_http);
                    $this->setNewFileLoc($fileLoc);
                    $newFileLoc = $fileLoc.$filepath['filename'].'.'.$format;
                } else {
                    $newFileLoc = $filepath['dirname'].'/'.$filepath['filename'].'.'.$format;
                }
                
                // CONVERT FILE TO DESIRE FORMAT
                $this->setFile($filename);
                $return = $this->convertFile($input, $output, $format, $ext);

                

                if ($return === 'csv') {
                    $filepath = pathinfo($input->getArgument('filename'));
                    $fileInfoLoc = $newFileLoc.$filepath['filename'].'.'.$format;
                    $output->writeln('<info>Conversion completed. Your can found you file here: </info>: '.$fileInfoLoc);
                    echo "\n";
                    return 1;
                }

                $output->writeln('<info>You have just selected</info>: '.$format);

                
                $output->writeln("<info>Conversion completed. Your can found you file here: </info>". $newFileLoc);

                // PUT TO FILE
                file_put_contents($newFileLoc, $return);
                break;
            
            default:
                // READ
                $output->writeln('<info> Reading the file. </info>');
                $read = file_get_contents($input->getArgument('filename'));
                $output->writeln($read);
                break;
        }

        echo "\n";
        return 1;
    }
}