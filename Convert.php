#!/usr/bin/env php
<?php

require_once __DIR__ . '/vendor/autoload.php';
use Symfony\Component\Console\Application;
use Console\ConvertCommand;

$app = new Application('Conversion App', 'v1.0.0');

$app -> add(new ConvertCommand());
$app -> run();